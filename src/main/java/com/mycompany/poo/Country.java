/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.poo;

/**
 * ENUM
 *
 * Un Enum est STATIC = ne peut pas être instancié (pas d'objet depuis un enum)
 * Peut contenir des attributs et des méthodes
 * Les attributs d'un enum sont des CONSTANTES
 * Les constantes sont PUBLIC, STATIC et FINAL
 * Pas d'héritage
 * Interfaces autorisées
 * 
 */
enum Country {
    BA,
    BE,
    DE,
    FR,
    LU,
    NL,
    RU,
    UK,
}

