/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.poo;

/**
 *
 * @author Binote
 */
public class Student extends User {
    
    public Student(String firstname, String lastname, String email, String phone, Country country) {
        super(firstname, lastname, email, phone, country);
    }
    
    public Student(String firstname, String lastname) {
        super(firstname, lastname);
    }
        
}
