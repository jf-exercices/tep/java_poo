/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.poo;

/**
 *
 * @author Binote
 */
public class User {
    
    // Pourquoi String et pas char ? (parce que String est une classe et on bénéficie de ses méthodes)
    private String firstname;
    private String lastname;
    private String email;
    private String phone;
    private Country country;
    private byte age;
    private byte level = 0;
    private boolean active = false;
    // prof = salary, hours, courses
    // student = hours, presences, courses

    // constructeur
    public User(String firstname, String lastname, String email, String phone, Country country) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.phone = phone;
        this.country = country;
    }

    public User(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public String getAttributes()
    {        
        return this.getFirstname()          // appel de la METHOD (GETTER) de l'INSTANCE de l'OBJET
                + " : " + getLastname()     // appel de la METHOD (GETTER) de la CLASSE
                + " : " + this.email        // l'email de l'INSTANCE de l'OBJET (this)
                + " : " + phone;            // le téléphone de la CLASSE (pas de this)
    }
    
    public void checkThis()
    {
        String firstname = "toto";
        
        System.out.println("=== checkThis method ===");
        System.out.println(firstname);
        System.out.println(this.firstname);
        System.out.println(this);
    }
        
    @Override
    public String toString() 
    {
        return String.format("Name: %s %s, Email : %s, Phone: %s", firstname, lastname, email, phone);
    }
    
  // Getters
    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public Country getCountry() {
        return country;
    }

    public byte getAge() {
        return age;
    }

    public byte getLevel() {
        return level;
    }

    public boolean isActive() {
        return active;
    }

   // Setters
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public void setAge(byte age) {
        this.age = age;
    }

    public void setLevel(byte level) {
        this.level = level;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    
    
  
  
    
}
