/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.poo;

/**
 *
 * @author Binote
 */
public class Teacher extends User {
    
    private int salary;
    
    public Teacher(String firstname, String lastname, String email, String phone, Country country, int salary) {
        super(firstname, lastname, email, phone, country);
        this.salary = salary;
    }
    
    public Teacher(String firstname, String lastname) {
        super(firstname, lastname);
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
    
    
    
    
    
    
    @Override
    public String getAttributes() {
        return super.getAttributes() + " : " + salary;
    }
        
}
