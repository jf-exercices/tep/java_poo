/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.poo;

/**
 *
 * @author Binote
 */
public class POO {

    public static void main(String[] args) {
        createTeacher();
        
    }
    
    public static void createUser() {
        // Instanciation de l'objet via le mot NEW
        // Instancier un objet exécute le CONSTRUCTEUR de la classe
        // Si le constructeur possède des paramètres, ils doivent être précisés lors de l'instanciation
        User oUser = new User("Michel", "Voneche", "michel@voneche.com", "0499123456", Country.BE);
        oUser.setLevel((byte)6); // CAST obligatoire car JAVA considère qu'une valeur numérique est de type INT par défaut.
/*        
        System.out.println(oUser.getFirstname());
        System.out.println(oUser.getLastname());
        System.out.println(oUser.getEmail());
        System.out.println(oUser.getPhone());
        System.out.println(oUser.getLevel());
        System.out.println(oUser.getAge());
*/
        // DRY : évitez la répétition pour afficher les attributs de l'objet
        System.out.println("Attributes of User => " + oUser.getAttributes());
        oUser.checkThis();        
    }
    
        
    public static void createTeacher() {
        Teacher oTeacher = new Teacher("Michel", "Voneche", "michel@voneche.com", "0499123456", Country.BE, 0);
        oTeacher.setSalary(60000);
        System.out.println(oTeacher.getSalary());        
    }

}
